#!/bin/bash
# Usage: . ./build_release_rpm.sh

# Build release RPM
echo "Building release RPM..."
rpmbuild --quiet --define "_topdir $PWD/release" --ba release/SPECS/*.spec
mv release/RPMS/*/*.rpm $PWD
ln -s *release*.rpm $CI_PROJECT_NAME-release.rpm
