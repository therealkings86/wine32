#!/bin/bash
# Usage: . ./rebuild_rpm.sh dir
dir=$1
shift
mkdir -p "$dir/rpmbuild"

# Custom spec file...
if [ -f $dir/custom/*.spec ]
then
  echo "(we are using a custom spec file)"
  rpm --define "_topdir $PWD/$dir/rpmbuild" -i $dir/*.src.rpm
  cp -f $dir/custom/*.spec $dir/rpmbuild/SPECS
  yum-builddep -q -y $dir/rpmbuild/SPECS/*.spec
  spectool -g -C $dir/rpmbuild/SOURCES $dir/rpmbuild/SPECS/*.spec
  linux32 rpmbuild --quiet --target=i686 --define "_topdir $PWD/$dir/rpmbuild" --define "dist .el7" --define "arch i686" --ba $dir/rpmbuild/SPECS/*.spec "$@"
# ...or just rebuild
else
  yum-builddep -q -y $dir/*.src.rpm
  linux32 rpmbuild --quiet --rebuild --target=i686 --define "_topdir $PWD/$dir/rpmbuild" --define "dist .el7" --define "arch i686" --ba $dir/*.src.rpm "$@"
fi
